import IRC from "irc";
import _ from "underscore";
import {permissions} from "../config";
import moment from "moment"

export default class Bot {

    // Constructor

    constructor(config) {
        this.Client = new IRC.Client(config.irc.server, config.irc.botName, {
            channels: config.irc.channels
        });
        this.BotDB = config.db.botDB;
        this.password = config.auth.password;
        this.isAuthenticated = false;
        this.commandOrder = config.commandOrder;
        this.args = [];
        this.from = '';
        this.to = '';
        this.commands = {
            cmds: {
                syntax: 'Syntaxe: !cmds',
                permission: permissions.ALL
            },
            source: {
                syntax: 'Syntaxe: !source',
                permission: permissions.ALL
            },
            logs: {
                syntax: 'Syntaxe: !logs',
                permission: permissions.IDENTIFIED
            },
        };
    }

    // IRC Server commands

    authenticate() {
        this.Client.say('NickServ', 'identify ' + this.password);
    }

    // Getters

    isCommand() {

        return this.args[1].startsWith(this.commandOrder)
            ? this.args[1].length !== 1
            : false;
    }

    commandExists() {

        this.BotDB.createLog(this.from, this.args[1]);

        let sentCommand = this.getCommandName();
        let found = _.find(this.commands, (command, key) => {
            return key.toLowerCase() === sentCommand.toLowerCase();
        });

        return !_.isUndefined(found);
    }

    getRecipient() {
        return this.to.indexOf('#') !== -1 ? this.to : this.from;
    }

    getCommandName() {
        return this.args[1].substring(1, this.args[1].length).split(' ')[0];
    }

    getUserInfo() {

        return new Promise(resolve => {
            this.Client.whois(this.from, info => {
                resolve(info);
            })
        });

    }

    async isUserIdentified() {

        let userInfo = await this.getUserInfo();
        return userInfo.hasOwnProperty('account') && userInfo.accountinfo === 'is logged in as';
    }

    // Command execute

    async commandExecute() {

        let sentCommand = this.getCommandName();
        let isBlacklisted = await this.BotDB.isBlacklisted(this.from);
        let isSpammed = await this.BotDB.findSpam(this.from);
        let recipient = this.getRecipient();

        if (isBlacklisted.length > 0) {
            return false;
        }

        if (isSpammed.length >= 3 && isBlacklisted.length === 0) {
            this.BotDB.addToBlacklist(this.from);
            this.Client.say(recipient, 'Détection de spam. Vous êtes ignoré pendant 2 minutes.');
        }

        if (this.commands[sentCommand].permission !== permissions.ALL) {
            this.isUserIdentified()
                .then(response => {
                    return response
                        ? this[sentCommand]()
                        : this.Client.say(recipient, 'Vous n\'avez pas la permission d\'utiliser cette commande.');
                })
                .catch(error => {
                    console.log(error);
            });
        }
        else {
            this[sentCommand]();
        }
    }

    // Bot Commands

    async logs() {

        let recipient = this.getRecipient();
        let logs = await this.BotDB.getLastLogs();

        this.Client.say(recipient, IRC.colors.wrap('underline', '10 dernières logs:'));

        for (let i = 0; i < 10; i++) {
            if (!_.isUndefined(logs[i])) {
                this.Client.say(recipient, logs[i].userName + ' ' + logs[i].command + ' ' + moment(logs[i].createdAt).format('DD-MM-YYYY HH:mm:ss'))
            }
        }
    }

    cmds() {

        let recipient = this.getRecipient();

        this.Client.say(recipient, IRC.colors.wrap('underline', 'Liste des commandes:'));
        _.each(this.commands, (cmd, index) => {
            this.Client.say(recipient, IRC.colors.wrap('bold', index));
            this.Client.say(recipient, cmd.syntax);
            this.Client.say(recipient, 'Permission : ' + cmd.permission);
            this.Client.say(recipient, '--');
        });
    }

    source() {
        this.Client.say(this.getRecipient(), 'https://bitbucket.org/rcouturier/mrrobot');
    }
}