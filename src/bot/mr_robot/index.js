
import Bot from '../../bot'
import {permissions} from "../../config";
import IRC from "irc";
import rp from "request-promise";
import cheerio from "cheerio";
import _ from "underscore";
import axios from "axios";
import moment from "moment"

export default class MrRobot extends Bot {

    // Constructor

    constructor(config) {
        super(config);
        this.commands.dtc = {
            syntax: 'Syntaxe: !dtc',
            permission: permissions.ALL
        };
        this.commands.pince = {
            syntax: 'Syntaxe: !pince',
            permission: permissions.ALL
        };
        this.commands.btc = {
            syntax: 'Syntaxe: !btc ' + IRC.colors.wrap('bold', '<monnaie>') + ' \r\n Syntaxe: !btc ' + IRC.colors.wrap('bold', '<monnaie>') +' convert ' + IRC.colors.wrap('bold', '<montant>'),
            permission: permissions.IDENTIFIED
        };
        this.commands.google = {
            syntax: 'Syntaxe: !google ' + IRC.colors.wrap('bold', '<recherche> <optionnel=site:url>'),
            permission: permissions.IDENTIFIED
        };
        this.commands.reminder = {
            syntax: 'Syntaxe: !reminder ' + IRC.colors.wrap('bold', '<add> <utilsateur> <message>')
                + '\r\n Syntaxe: !reminder ' + IRC.colors.wrap('bold', '<del> <utilsateur>')
                + '\r\n Syntaxe: !reminder ' + IRC.colors.wrap('bold', '<read>'),
            permission: permissions.IDENTIFIED
        };
    }

    async sendUserReminder(recipient, userName) {

        let reminders = await this.BotDB.getUserReminders(userName);

        if (reminders.length > 0) {

            let read = _.filter(reminders, reminder => {
                return reminder.status === 'read';
            });

            let unread = _.filter(reminders, reminder => {
                return reminder.status === 'unread';
            });

            this.Client.say(recipient, 'Vous avez ' + unread.length + ' nouveau(x) message(s) et ' + read.length + ' message(s) lu(s)');
            this.Client.say(recipient, 'Pour les lire, tappez ' + IRC.colors.wrap('bold', '!reminder read'));
        }
    }

    // Bot Commands

    dtc() {
        this.Client.say(this.getRecipient(), 'B=====D');
    }

    pince() {
        this.Client.say(this.getRecipient(), '-<');
    }

    async reminder() {

        let recipient = this.getRecipient();
        let message = this.args[1].substring(1, this.args[1].length).split(' ');

        let command = message[0];
        let subCommand = message[1];
        let reminderTo = message[2];
        let reminderMsg = message[3];

        if (!_.isUndefined(subCommand)
            && subCommand.toLowerCase() === 'add'
            && !_.isUndefined(reminderTo)
            && !_.isUndefined(reminderMsg)) {

            let substr = (command.length + 2) + (subCommand.length + 1) + (reminderTo.length + 1);

            reminderMsg = this.args[1].substring(substr, this.args[1].length);

            let existingReminder = await this.BotDB.getUserReminders(reminderTo, this.from);

            if (existingReminder.length >= 3) {
                this.Client.say(recipient, 'La boite de l\'utilisateur ' + reminderTo + ' est pleine!');
                return false;
            }

            existingReminder.length > 0
                ? await this.BotDB.updateUserReminder(this.from, reminderTo, reminderMsg)
                : await this.BotDB.createUserReminder(this.from, reminderTo, reminderMsg);

            this.Client.say(recipient, 'Votre message est bien sauvegardé.');
        }
        else if (!_.isUndefined(subCommand)
            && subCommand.toLowerCase() === 'read'
            && _.isUndefined(reminderTo)
            && _.isUndefined(reminderMsg)) {

            let existingReminder = await this.BotDB.getUserReminders(this.from);

            if (existingReminder.length === 0) {
                this.Client.say(recipient, 'Vous n\'avez aucun messages.');
            }

            _.each(existingReminder, reminder => {
                this.Client.say(recipient, IRC.colors.wrap('underline', 'Envoyé par: ' + reminder.from + ' le ' + moment(reminder.createdAt).format('DD-MM-YYYY HH:mm:ss')));
                this.Client.say(recipient, 'Status: ' + reminder.status);
                this.Client.say(recipient, reminder.message);
                this.Client.say(recipient, '--');
                this.BotDB.readUserReminder(reminder.from, reminder.to);
            });

        }
        else if (!_.isUndefined(subCommand)
            && subCommand.toLowerCase() === 'del'
            && !_.isUndefined(reminderTo)
            && _.isUndefined(reminderMsg)) {

            let existingReminder = await this.BotDB.getUserReminders(reminderTo, this.from);

            if (existingReminder.length > 0) {
                this.BotDB.deleteUserReminder(this.from, reminderTo);
                this.Client.say(recipient, 'Votre message est bien supprimé.');
            }

        }
        else {
            this.Client.say(recipient, 'Commande incomplète.');
            this.Client.say(recipient, this.commands[command].syntax);
        }
    }

    google() {

        let message = this.args[1];
        let recipient = this.getRecipient();
        let googleUrl = 'https://www.google.com/search';

        let excludeCommand = new RegExp(message.split(' ')[0], 'g');
        message = message.replace(excludeCommand, '')
            .substring(1, message.length)
            .replace(/\s/g,'+');

        rp.get({uri: googleUrl + '?q=' + message + '&oq=' + message, encoding: 'latin1'})
            .then(html => {
                let $ = cheerio.load(html);
                $('.g .r').each( (index, el) => {
                    let url = decodeURIComponent($(el).find('> a').attr('href')).split('&sa')[0];
                    this.Client.say(recipient, IRC.colors.wrap('bold', $(el).text()));
                    this.Client.say(recipient, $(el).parent().find('.s .st').text());
                    this.Client.say(recipient, url.substring(7,url.length) + '\r\n');
                    this.Client.say(recipient, '-------------');
                });
            })
            .catch(error => {
                console.log(error)
            })

    }

    btc() {

        let recipient = this.getRecipient();
        let message = this.args[1].substring(1, this.args[1].length).split(' ');

        let command = message[0];
        let currency = message[1];
        let subcommand = message[2];
        let amount = message[3];

        if (!_.isUndefined(currency) && _.isUndefined(subcommand)) {

            currency = currency.toUpperCase();

            axios.get('https://api.coindesk.com/v1/bpi/currentprice/'+ currency + '.json')
                .then(response => {
                    this.Client.say(recipient, IRC.colors.wrap('underline','Prix du bitcoin :'));
                    this.Client.say(recipient, 'Dernière mise à jour : ' + response.data.time.updated);
                    this.Client.say(recipient, IRC.colors.wrap('bold', '1 BTC = ' + response.data.bpi[currency].rate + ' ' + response.data.bpi[currency].description));
                })
                .catch(error => {
                    this.Client.say(recipient, 'Erreur : ' + error.response.data);
                })
        }
        else if (!_.isUndefined(subcommand) && !_.isUndefined(amount) && subcommand.toLowerCase() === 'convert') {

            currency = currency.toUpperCase();

            axios.get('https://api.coindesk.com/v1/bpi/currentprice/'+ currency + '.json')
                .then(response => {
                    let rate = parseFloat(response.data.bpi[currency].rate.replace(/,/g, ''));
                    this.Client.say(recipient, 'Dernière mise à jour : ' + response.data.time.updated);
                    this.Client.say(recipient, IRC.colors.wrap('bold', amount + ' ' + currency + ' = ' + parseFloat(amount/rate).toFixed(7) + ' BTC'));
                })
                .catch(error => {
                    this.Client.say(recipient, 'Erreur : ' + error.response.data);
                })

        }
        else {
            this.Client.say(recipient, 'Commande incomplète.');
            this.Client.say(recipient, this.commands[command].syntax);
        }

    }

}