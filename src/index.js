
import { bots } from './config'
import MrRobot from './bot/mr_robot'

const Alpha = new MrRobot(bots.alpha);

Alpha.Client.on("message", (nick, to, text, message) => {

    Alpha.from = nick;
    Alpha.to = to;
    Alpha.args = message.args;

    if (Alpha.isCommand()) {

        Alpha.commandExists()
            ? Alpha.commandExecute()
            : Alpha.Client.say(Alpha.getRecipient(), 'Commande inconnue.');
    }

});

Alpha.Client.on("join", (channel, who) => {
    if (bots.alpha.irc.botName === who && !Alpha.isAuthenticated) {
        Alpha.authenticate();
    }
    Alpha.sendUserReminder(channel, who);
});

Alpha.Client.on('error', message => {
    console.log('error: ', message);
});

Alpha.Client.on("registered", message => {
    console.log(message)
});