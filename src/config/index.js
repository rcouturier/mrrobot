
import Sequelize from "sequelize";
import MrRobotDB from "../db/mr_robot";

const Op = Sequelize.Op;

// MySQL DB config

export const operatorsAliases = {
    $eq: Op.eq,
    $ne: Op.ne,
    $gte: Op.gte,
    $gt: Op.gt,
    $lte: Op.lte,
    $lt: Op.lt,
    $not: Op.not,
    $in: Op.in,
    $notIn: Op.notIn,
    $is: Op.is,
    $like: Op.like,
    $notLike: Op.notLike,
    $iLike: Op.iLike,
    $notILike: Op.notILike,
    $regexp: Op.regexp,
    $notRegexp: Op.notRegexp,
    $iRegexp: Op.iRegexp,
    $notIRegexp: Op.notIRegexp,
    $between: Op.between,
    $notBetween: Op.notBetween,
    $overlap: Op.overlap,
    $contains: Op.contains,
    $contained: Op.contained,
    $adjacent: Op.adjacent,
    $strictLeft: Op.strictLeft,
    $strictRight: Op.strictRight,
    $noExtendRight: Op.noExtendRight,
    $noExtendLeft: Op.noExtendLeft,
    $and: Op.and,
    $or: Op.or,
    $any: Op.any,
    $all: Op.all,
    $values: Op.values,
    $col: Op.col
};

// Bots list config

export const bots = {
    alpha: {
        commandOrder: '!',
        auth: {
            password: 'R0b0tR0cks'
        },
        db: {
            botDB: new MrRobotDB({
                host: 'localhost',
                database: 'mr_robot',
                userName: 'root',
                password: 'root',
                dialect: 'mysql',
                operatorsAliases: operatorsAliases,
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000
                }
            })
        },
        irc: {
            channels: ["#family"],
            server: "192.168.1.17",
            botName: "MrRobot",
            autoConnect: false,
            encoding: 'utf-8',
        },
    }
};

export const permissions = {
    ALL: 'all',
    IDENTIFIED: 'identified'
};