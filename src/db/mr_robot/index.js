import Sequelize from "sequelize";
import _ from "underscore";
import moment from "moment"
import BotDB from "../index";

export default class MrRobotDB extends BotDB {

    constructor(dbConfig) {
        super(dbConfig);
        this.tables.reminders = this.Client.define('reminders', {
            from: {
                type: Sequelize.STRING
            },
            to: {
                type: Sequelize.STRING
            },
            message: {
                type: Sequelize.TEXT
            },
            status: {
                type: Sequelize.ENUM('unread', 'read'),
                defaultValue: 'unread'
            }
        });

        this.tables.reminders.sync({force: true});
    }

    createUserReminder(from, to, message) {
        return this.tables.reminders.create({
            from: from,
            to: to,
            message: message
        })
    }

    readUserReminder(from, to) {
        return this.tables.reminders.update({
            status: 'read'
        },{
            where: {
                from: from,
                to: to,
            }
        })
    }

    updateUserReminder(from, to, message) {
        return this.tables.reminders.update({
            createdAt: moment().toDate(),
            message: message,
            status: 'unread'
        },{
            where: {
                from: from,
                to: to,
            }
        })
    }

    deleteUserReminder(from, to) {
        return this.tables.reminders.destroy({
            where: {
                from: from,
                to: to
            }
        })
    }

    getUserReminders(to, from) {
        let req = {
            where: {
                to: to,
            }
        };

        if (!_.isUndefined(from))
            req.where.from = from;

        return this.tables.reminders.findAll(req);
    }

}