import Sequelize from "sequelize";
import moment from "moment";

export default class BotDB {

    constructor(dbConfig) {
        this.Client = new Sequelize(dbConfig.database, dbConfig.userName, dbConfig.password, {
            host: dbConfig.host,
            dialect: dbConfig.dialect,
            operatorsAliases: dbConfig.operatorsAliases,
            pool: dbConfig.pool,
        });

        this.hasLogs = false;
        this.hasBlacklistUsers = false;

        // Base tables for all bots
        this.tables = {};
        this.tables.logs = this.Client.define('logs', {
            userName: {
                type: Sequelize.STRING
            },
            command: {
                type: Sequelize.STRING
            }
        });
        this.tables.blackList = this.Client.define('blackList', {
            userName: {
                type: Sequelize.STRING
            },
        });
        this.tables.logs.sync({force: true});
        this.tables.blackList.sync({force: true});

        // Cron for clearing logs & blacklists
        setInterval(
            () => {
                if (this.hasBlacklistUsers)
                    this.tables.blackList.destroy({truncate: true});
                return this.hasBlacklistUsers = false;
            }, 120000
        );
        setInterval(
            () => {
                if (this.hasLogs)
                    this.tables.logs.destroy({truncate: true});
                return this.hasLogs = false;
            }, 600000
        );
    }

    getLastLogs() {
        return this.tables.logs.findAll({limit: 10, order: [['updatedAt', 'DESC']]});
    }

    findSpam(userName) {
        let limit = moment().subtract(2, 'seconds').toDate();
        let now = moment().toDate();

        return this.tables.logs.findAll({
            attributes: ['createdAt'],
            where: {
                userName: userName,
                createdAt: {$between: [limit, now]}
            }
        });
    }

    isBlacklisted(userName) {
        return this.tables.blackList.findAll({
            attributes: ['userName'],
            where: {
                userName: userName,
            }
        });
    }

    createLog(userName, command) {
        this.tables.logs.create({
            userName: userName,
            command: command
        });
        this.hasLogs = true;
    }

    addToBlacklist(userName) {
        this.tables.blackList.create({
            userName: userName,
        });
        this.hasBlacklistUsers = true;
    }

};